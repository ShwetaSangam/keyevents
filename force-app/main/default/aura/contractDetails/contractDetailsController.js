({
    myAction : function(component, event, helper) {
        component.set("v.hideNext",true);
        
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            type:"Success",
            "message": "The record has been updated successfully."
        });
        toastEvent.fire();

    },
    gotoURL : function(component, event, helper) {
        var id = component.get('v.recordId');
        var url ='/apex/Contract_VF_page?id='+id; 
        window.open(url,'_blank');
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
         },

     nextPage:function(component, event, helper)
     {
        component.set("v.outcome",false);
        component.set("v.outcome2",true);
     },
     
})