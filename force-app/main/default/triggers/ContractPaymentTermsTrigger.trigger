trigger ContractPaymentTermsTrigger on Opportunity (after update, after insert) {
    
    // Set to store all the inserted or updated opportunities
    Set<Id> oppIdSet = new Set<Id>();
    Set<Id> accountIdSet = new Set<Id>();
    
    if(Trigger.IsUpdate){
        for(Opportunity opp : trigger.new) 
        {
            if(opp.StageName == 'Contracted'){
            Opportunity oldOpp = Trigger.oldMap.get(opp.Id);
            
            // Compare old values and new values of Contract Type and Payment Terms
            if((opp.Contract_Type__c != null 
                && opp.Payment_Term__c != null
                && opp.Contract_Type__c.equals(oldOpp.Contract_Type__c) 
                && opp.Payment_Term__c.equals(oldOpp.Payment_Term__c)
                && opp.Percentage_No_1__c == oldOpp.Percentage_No_1__c
                && opp.Percentage_No_2__c == oldOpp.Percentage_No_2__c
                && opp.Percentage_No_3__c == oldOpp.Percentage_No_3__c
                && opp.Percentage_No_4__c == oldOpp.Percentage_No_4__c
                && opp.Percentage_No_5__c == oldOpp.Percentage_No_5__c)
               || (opp.Contract_Type__c == null 
                   && opp.Payment_Term__c == null)){
                       continue;
                   }
            
            oppIdSet.add(opp.Id);
            accountIdSet.add(opp.AccountId);
        }
            }
    }
    
    if(Trigger.IsInsert){
        for(Opportunity opp : trigger.new){
            oppIdSet.add(opp.Id);
            accountIdSet.add(opp.AccountId);
        }
    }
    
    if(oppIdSet.isEmpty()) {
        System.debug('No change in Contract Type and Payment Terms, so exiting trigger');
        return;
    }
    
    // Querying all the related Invoices
    List<Invoice__c> invoiceList = [Select Name, Invoice_Date__c, Opportunity__c, Account__c from Invoice__c where Opportunity__r.Id IN: oppIdSet];
    
    // Map to group Opportunity and its related Invoices
    Map<Id, List<Invoice__c>> oppIdInvoiceMap = new Map<Id, List<Invoice__c>>();
    
    for(Invoice__c invObj : invoiceList) {
        
        List<Invoice__c> invList = null;
        
        if(oppIdInvoiceMap.containsKey(invObj.Opportunity__c)) {
            invList = oppIdInvoiceMap.get(invObj.Opportunity__c);
        } else {
            invList = new List<Invoice__c>();
        }
        
        invList.add(invObj);
        oppIdInvoiceMap.put(invObj.Opportunity__c, invList);
    }
    
    // Querying all the related Accounts
    Map<Id, Account> accMap = new Map<Id, Account>([Select Id, BillingCity, BillingState, BillingCountry, BillingPostalCode from Account where Id IN: accountIdSet]);
    
    List<Invoice__c> invoiceDeleteList = new List<Invoice__c>();
    List<Invoice__c> invoiceCreateList = new List<Invoice__c>();
    Map<Id, Integer> oppIdPaymentTermsMap = new Map<Id, Integer>();
    
    for(Opportunity opp : trigger.new) {
        Opportunity oldOpp1 = new Opportunity();
        if(Trigger.IsUpdate){
            oldOpp1 = Trigger.oldMap.get(opp.Id);
        }
        
        if((opp.Contract_Type__c == 'Percentage' && (opp.Percentage_No_1__c == null || opp.Percentage_No_1__c <= 0))
           || (opp.Contract_Type__c == 'Percentage' && opp.Payment_Term__c != oldOpp1.Payment_Term__c)){
               continue;
           }
        else if(opp.Contract_Type__c == 'Percentage' && opp.Percentage_No_1__c > 0){
            List<Invoice__c> relatedInvoices = oppIdInvoiceMap.get(opp.Id);
            
            // Delete Invoices related to the opportunity
            if(relatedInvoices != null && !relatedInvoices.isEmpty()) {
                invoiceDeleteList.addAll(relatedInvoices);
            }
            
            Integer paymentTerms = Integer.valueOf(opp.Payment_Term__c);
            oppIdPaymentTermsMap.put(opp.Id, paymentTerms);
            
            // Based on number of payment Terms, create those many number of Invoice's
            for(Integer i = 0 ; i < paymentTerms ; i++) {
                Invoice__c invObj = new Invoice__c();
                invObj.Invoice_Status__c = 'Draft';
                invObj.Opportunity__c = opp.Id;
                invObj.account__c = opp.AccountId;
                invObj.Invoice_Date__c =  opp.CloseDate;
                
                if(i==0){
                    invObj.Invoice_Amount__c = (opp.Percentage_No_1__c * opp.Estimated_Amount__c)/100;
                }else if(i==1){
                    invObj.Invoice_Amount__c = (opp.Percentage_No_2__c * opp.Estimated_Amount__c)/100;
                }else if(i==2){
                    invObj.Invoice_Amount__c = (opp.Percentage_No_3__c * opp.Estimated_Amount__c)/100;
                }else if(i==3){
                    invObj.Invoice_Amount__c = (opp.Percentage_No_4__c * opp.Estimated_Amount__c)/100;
                }else if(i==4){
                    invObj.Invoice_Amount__c = (opp.Percentage_No_5__c * opp.Estimated_Amount__c)/100;
                }
                invoiceCreateList.add(invObj);
            }
        }else{
            
            List<Invoice__c> relatedInvoices = oppIdInvoiceMap.get(opp.Id);
            
            // Delete Invoices related to the opportunity
            if(relatedInvoices != null && !relatedInvoices.isEmpty()) {
                invoiceDeleteList.addAll(relatedInvoices);
            }
            
            Integer paymentTerms;
            if(opp.Payment_Term__c != null)
            {
                paymentTerms = Integer.valueOf(opp.Payment_Term__c);
                oppIdPaymentTermsMap.put(opp.Id, paymentTerms);
            }
            
            // Based on number of payment Terms, create those many number of Invoice's
            for(Integer i = 0 ; i < paymentTerms ; i++) {
                Invoice__c invObj = new Invoice__c();
                invObj.Invoice_Status__c = 'Draft';
                invObj.Opportunity__c = opp.Id;
                invObj.account__c = opp.AccountId;
                invObj.Invoice_Amount__c = (opp.Estimated_Amount__c / paymentTerms);
                
                if(paymentTerms == 1){
                    invObj.Invoice_Date__c =  opp.CloseDate;
                }else if(paymentTerms == 2){
                    if(i == 0){
                        invObj.Invoice_Date__c =  opp.CloseDate.addMonths(i);
                    }else{
                        invObj.Invoice_Date__c =  opp.CloseDate.addMonths(i*6);
                    }
                }else if(paymentTerms == 3){
                    if(i == 0){
                        invObj.Invoice_Date__c =  opp.CloseDate.addMonths(i);
                    }else{
                        invObj.Invoice_Date__c =  opp.CloseDate.addMonths(i*4);
                    }
                }else if(paymentTerms == 4){
                    if(i == 0){
                        invObj.Invoice_Date__c =  opp.CloseDate.addMonths(i);
                    }else{
                        invObj.Invoice_Date__c =  opp.CloseDate.addMonths(i*3);
                    }
                }else if(paymentTerms == 6){
                    if(i == 0){
                        invObj.Invoice_Date__c =  opp.CloseDate.addMonths(i);
                    }else{
                        invObj.Invoice_Date__c =  opp.CloseDate.addMonths(i*2);
                    }
                }else if(paymentTerms == 12){
                    invObj.Invoice_Date__c =  opp.CloseDate.addMonths(i);
                }else{
                    invObj.Invoice_Date__c =  opp.CloseDate.addMonths(i);
                }
                
                invoiceCreateList.add(invObj);
            }
        }
    }
    
    if(!invoiceDeleteList.isEmpty()) {
        delete invoiceDeleteList;
    }
    
    if(!invoiceCreateList.isEmpty()) {
        insert invoiceCreateList;
    }
    
}