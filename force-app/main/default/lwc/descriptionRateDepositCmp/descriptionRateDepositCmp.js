import { LightningElement,api, track, wire } from 'lwc';
import fetchInvoice from '@salesforce/apex/DescriptionRateDepositCmpHelper.fetchFields';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import NAME_FIELD from '@salesforce/schema/Invoice__c.Name';
import FINALINVOICE_FIELD from '@salesforce/schema/Invoice__c.Is_it_a_final_Invoice_for_this_Contact__c';
import NOOFDEPOSIT_FIELD from '@salesforce/schema/Invoice__c.Number_of_Deposit__c';
import NOOFLINEITEM_FIELD from '@salesforce/schema/Invoice__c.Number_of_Line_Items__c';


 
export default class DescriptionRateDepositCmp extends LightningElement {
 @api recordId;
 @track record;
 @track error;


 @wire(getRecord, { recordId: '$recordId', fields: [NOOFDEPOSIT_FIELD,NOOFLINEITEM_FIELD,FINALINVOICE_FIELD] })
 account;

 


 get showTemplate() {
    
     if(getFieldValue(this.account.data, FINALINVOICE_FIELD) == 'Yes')
    return true;
    else
    return false;
}


get showDeposit1()
{
    if(getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '1' || getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '2' || getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '3'
    || getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '4' || getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '5'
    || getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '6' || getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '7'
    || getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '8' || getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '9'
    || getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '10') 
    return true;
    else
    return false;
}




get showDeposit2()
{
    if(getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '2' || getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '3'
    || getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '4' || getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '5'
    || getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '6' || getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '7'
    || getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '8' || getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '9'
    || getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '10') 
    return true;
    else
    return false;
}


get showDeposit3()
{
    if(getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '3'|| getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '4' || getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '5'
    || getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '6' || getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '7'
    || getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '8' || getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '9'
    || getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '10') 
    return true;
    else
    return false;
}


get showDeposit4()
{
    if(getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '4'|| getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '5' || getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '6'
    || getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '7' || getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '8'
    || getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '9' || getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '10') 
    return true;
    else
    return false;
}

get showDeposit5()
{
    if(getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '5'|| getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '6' || getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '7'
    || getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '8' || getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '9'
    || getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '10') 
    return true;
    else
    return false;
}

get showDeposit6()
{
    if(getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '6'|| getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '7' || getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '8'
    || getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '9' || getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '10') 
    return true;
    else
    return false;
}

get showDeposit7()
{
    if(getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '7'|| getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '8' || getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '9'
    || getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '10') 
    return true;
    else
    return false;
}

get showDeposit8()
{
    if(getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '8'|| getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '9' || getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '10') 
    return true;
    else
    return false;
}

get showDeposit9()
{
    if(getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '9'|| getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '10') 
    return true;
    else
    return false;
}

get showDeposit10()
{
    if(getFieldValue(this.account.data, NOOFDEPOSIT_FIELD) == '10') 
    return true;
    else
    return false;
}

get showDescripAndRate2() {
    if(getFieldValue(this.account.data, NOOFLINEITEM_FIELD) == '2' || getFieldValue(this.account.data, NOOFLINEITEM_FIELD) == '3' || getFieldValue(this.account.data, NOOFLINEITEM_FIELD) == '4'
    || getFieldValue(this.account.data, NOOFLINEITEM_FIELD) == '5' || getFieldValue(this.account.data, NOOFLINEITEM_FIELD) == '6' || getFieldValue(this.account.data, NOOFLINEITEM_FIELD) == '7'
    || getFieldValue(this.account.data, NOOFLINEITEM_FIELD) == '8' || getFieldValue(this.account.data, NOOFLINEITEM_FIELD) == '9' || getFieldValue(this.account.data, NOOFLINEITEM_FIELD) == '10')
    return true;
    else
    return false;
}

get showDescripAndRate3() {
    if(getFieldValue(this.account.data, NOOFLINEITEM_FIELD) == '3' || getFieldValue(this.account.data, NOOFLINEITEM_FIELD) == '4' || getFieldValue(this.account.data, NOOFLINEITEM_FIELD) == '5'
    || getFieldValue(this.account.data, NOOFLINEITEM_FIELD) == '6' || getFieldValue(this.account.data, NOOFLINEITEM_FIELD) == '7' || getFieldValue(this.account.data, NOOFLINEITEM_FIELD) == '8'
    || getFieldValue(this.account.data, NOOFLINEITEM_FIELD) == '9' || getFieldValue(this.account.data, NOOFLINEITEM_FIELD) == '10')
    return true;
    else
    return false;
}

get showDescripAndRate4() {
    if(getFieldValue(this.account.data, NOOFLINEITEM_FIELD) == '4' || getFieldValue(this.account.data, NOOFLINEITEM_FIELD) == '5' || getFieldValue(this.account.data, NOOFLINEITEM_FIELD) == '6'
    || getFieldValue(this.account.data, NOOFLINEITEM_FIELD) == '7' || getFieldValue(this.account.data, NOOFLINEITEM_FIELD) == '8' || getFieldValue(this.account.data, NOOFLINEITEM_FIELD) == '9'
    || getFieldValue(this.account.data, NOOFLINEITEM_FIELD) == '10')
    return true;
    else
    return false;
}

get showDescripAndRate5() {
    if(getFieldValue(this.account.data, NOOFLINEITEM_FIELD) == '5' || getFieldValue(this.account.data, NOOFLINEITEM_FIELD) == '6' || getFieldValue(this.account.data, NOOFLINEITEM_FIELD) == '7'
    || getFieldValue(this.account.data, NOOFLINEITEM_FIELD) == '8' || getFieldValue(this.account.data, NOOFLINEITEM_FIELD) == '9' || getFieldValue(this.account.data, NOOFLINEITEM_FIELD) == '10')
    return true;
    else
    return false;
}

get showDescripAndRate6() {
    if(getFieldValue(this.account.data, NOOFLINEITEM_FIELD) == '6' || getFieldValue(this.account.data, NOOFLINEITEM_FIELD) == '7' || getFieldValue(this.account.data, NOOFLINEITEM_FIELD) == '8'
    || getFieldValue(this.account.data, NOOFLINEITEM_FIELD) == '9' || getFieldValue(this.account.data, NOOFLINEITEM_FIELD) == '10')
    return true;
    else
    return false;
}


get showDescripAndRate7() {
    if(getFieldValue(this.account.data, NOOFLINEITEM_FIELD) == '7' || getFieldValue(this.account.data, NOOFLINEITEM_FIELD) == '8' || getFieldValue(this.account.data, NOOFLINEITEM_FIELD) == '9'
    || getFieldValue(this.account.data, NOOFLINEITEM_FIELD) == '10')
    return true;
    else
    return false;
}


get showDescripAndRate8() {
    if(getFieldValue(this.account.data, NOOFLINEITEM_FIELD) == '8' || getFieldValue(this.account.data, NOOFLINEITEM_FIELD) == '9' || getFieldValue(this.account.data, NOOFLINEITEM_FIELD) == '10')
    return true;
    else
    return false;
}


get showDescripAndRate9() {
    if(getFieldValue(this.account.data, NOOFLINEITEM_FIELD) == '9' || getFieldValue(this.account.data, NOOFLINEITEM_FIELD) == '10')
    return true;
    else
    return false;
}
get showDescripAndRate10() {
    if(getFieldValue(this.account.data, NOOFLINEITEM_FIELD) == '10')
    return true;
    else
    return false;
}


}