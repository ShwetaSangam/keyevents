import { LightningElement,api, track, wire } from 'lwc';

import fetchInvoice from '@salesforce/apex/DescriptionRateDepositCmpHelper.fetchFields';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import Number_Of_Installments from '@salesforce/schema/Opportunity.Payment_Term__c';

export default class ContractDetails extends LightningElement {
    
    @api recordId;
    @track record;
    @track error;

 @wire(getRecord, { recordId: '$recordId', fields: [Number_Of_Installments] })
 account;
    
 get showDeposit1()
{
    if(getFieldValue(this.account.data, Number_Of_Installments) == '1' || getFieldValue(this.account.data, Number_Of_Installments) == '2'
    || getFieldValue(this.account.data, Number_Of_Installments) == '3' || getFieldValue(this.account.data, Number_Of_Installments) == '4'
    || getFieldValue(this.account.data, Number_Of_Installments) == '5' || getFieldValue(this.account.data, Number_Of_Installments) == '6'
    || getFieldValue(this.account.data, Number_Of_Installments) == '7' || getFieldValue(this.account.data, Number_Of_Installments) == '8'
    || getFieldValue(this.account.data, Number_Of_Installments) == '9' || getFieldValue(this.account.data, Number_Of_Installments) == '10'
    || getFieldValue(this.account.data, Number_Of_Installments) == '11' || getFieldValue(this.account.data, Number_Of_Installments) == '12') 
    return true;
    else
    return false;
}

get showDeposit2()
{
    if(getFieldValue(this.account.data, Number_Of_Installments) == '2' || getFieldValue(this.account.data, Number_Of_Installments) == '3'
    || getFieldValue(this.account.data, Number_Of_Installments) == '4' || getFieldValue(this.account.data, Number_Of_Installments) == '5'
    || getFieldValue(this.account.data, Number_Of_Installments) == '6' || getFieldValue(this.account.data, Number_Of_Installments) == '7'
    || getFieldValue(this.account.data, Number_Of_Installments) == '8' || getFieldValue(this.account.data, Number_Of_Installments) == '9'
    || getFieldValue(this.account.data, Number_Of_Installments) == '10' || getFieldValue(this.account.data, Number_Of_Installments) == '11'
    || getFieldValue(this.account.data, Number_Of_Installments) == '12') 
    return true;
    else
    return false;
}

get showDeposit3()
{
    if(getFieldValue(this.account.data, Number_Of_Installments) == '3' || getFieldValue(this.account.data, Number_Of_Installments) == '4'
    || getFieldValue(this.account.data, Number_Of_Installments) == '5' || getFieldValue(this.account.data, Number_Of_Installments) == '6'
    || getFieldValue(this.account.data, Number_Of_Installments) == '7' || getFieldValue(this.account.data, Number_Of_Installments) == '8'
    || getFieldValue(this.account.data, Number_Of_Installments) == '9' || getFieldValue(this.account.data, Number_Of_Installments) == '10'
    || getFieldValue(this.account.data, Number_Of_Installments) == '11' || getFieldValue(this.account.data, Number_Of_Installments) == '12') 
    return true;
    else
    return false;
}

get showDeposit4()
{
    if(getFieldValue(this.account.data, Number_Of_Installments) == '4' || getFieldValue(this.account.data, Number_Of_Installments) == '5'
    || getFieldValue(this.account.data, Number_Of_Installments) == '6' || getFieldValue(this.account.data, Number_Of_Installments) == '7'
    || getFieldValue(this.account.data, Number_Of_Installments) == '8' || getFieldValue(this.account.data, Number_Of_Installments) == '9'
    || getFieldValue(this.account.data, Number_Of_Installments) == '10' || getFieldValue(this.account.data, Number_Of_Installments) == '11'
    || getFieldValue(this.account.data, Number_Of_Installments) == '12') 
    return true;
    else
    return false;
}

get showDeposit5()
{
    if(getFieldValue(this.account.data, Number_Of_Installments) == '5' || getFieldValue(this.account.data, Number_Of_Installments) == '6'
    || getFieldValue(this.account.data, Number_Of_Installments) == '7' || getFieldValue(this.account.data, Number_Of_Installments) == '8'
    || getFieldValue(this.account.data, Number_Of_Installments) == '9' || getFieldValue(this.account.data, Number_Of_Installments) == '10'
    || getFieldValue(this.account.data, Number_Of_Installments) == '11' || getFieldValue(this.account.data, Number_Of_Installments) == '12') 
    return true;
    else
    return false;
}

get showDeposit6()
{
    if(getFieldValue(this.account.data, Number_Of_Installments) == '6' || getFieldValue(this.account.data, Number_Of_Installments) == '7'
    || getFieldValue(this.account.data, Number_Of_Installments) == '8' || getFieldValue(this.account.data, Number_Of_Installments) == '9'
    || getFieldValue(this.account.data, Number_Of_Installments) == '10' || getFieldValue(this.account.data, Number_Of_Installments) == '11'
    || getFieldValue(this.account.data, Number_Of_Installments) == '12') 
    return true;
    else
    return false;
}

get showDeposit7()
{
    if(getFieldValue(this.account.data, Number_Of_Installments) == '7' || getFieldValue(this.account.data, Number_Of_Installments) == '8'
    || getFieldValue(this.account.data, Number_Of_Installments) == '9' || getFieldValue(this.account.data, Number_Of_Installments) == '10'
    || getFieldValue(this.account.data, Number_Of_Installments) == '11' || getFieldValue(this.account.data, Number_Of_Installments) == '12') 
    return true;
    else
    return false;
}

get showDeposit8()
{
    if(getFieldValue(this.account.data, Number_Of_Installments) == '8' || getFieldValue(this.account.data, Number_Of_Installments) == '9'
    || getFieldValue(this.account.data, Number_Of_Installments) == '10' || getFieldValue(this.account.data, Number_Of_Installments) == '11'
    || getFieldValue(this.account.data, Number_Of_Installments) == '12') 
    return true;
    else
    return false;
}

get showDeposit9()
{
    if(getFieldValue(this.account.data, Number_Of_Installments) == '9' || getFieldValue(this.account.data, Number_Of_Installments) == '10'
    || getFieldValue(this.account.data, Number_Of_Installments) == '11' || getFieldValue(this.account.data, Number_Of_Installments) == '12') 
    return true;
    else
    return false;
}

get showDeposit10()
{
    if(getFieldValue(this.account.data, Number_Of_Installments) == '10' || getFieldValue(this.account.data, Number_Of_Installments) == '11'
    || getFieldValue(this.account.data, Number_Of_Installments) == '12') 
    return true;
    else
    return false;
}

get showDeposit11()
{
    if(getFieldValue(this.account.data, Number_Of_Installments) == '11' || getFieldValue(this.account.data, Number_Of_Installments) == '12') 
    return true;
    else
    return false;
}

get showDeposit12()
{
    if(getFieldValue(this.account.data, Number_Of_Installments) == '12') 
    return true;
    else
    return false;
}

}