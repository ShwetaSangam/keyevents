global class qb_customercreation_scheduler implements Schedulable{
    global void execute(SchedulableContext sc){
        Database.executeBatch(new qb_customercreation_batch(), 1);       
    }
}