public class qb_invoice_create implements Queueable, Database.AllowsCallouts{
    
    private ID RecordId;
    private String ObjectType;
    public String AccessToken;
    public String CompanyId;
    public String URL;
    public String Status;
    public String Sync; 
    
    
    public qb_invoice_create(Id recId, String stat){
        RecordId = recId;
        Status = stat;
    }
    
    
    public void execute(QueueableContext context) {       
        Invoice__c Inv = [Select Id, 
                       Name,
                       Account__c,
                       Account__r.QB_ID__c,
                       Account__r.Name,
                       QB_ID__c
                       from Invoice__c where Id =: RecordId limit 1];     

        List<Invoice_Line_Item__c> LineItems = new List<Invoice_Line_Item__c>([Select Id,
                                                 Name,
                                                 Amount__c,
                                                 Service__c
                                                 from Invoice_Line_Item__c where Invoice__c =: RecordId ORDER BY Amount__c ASC]);   
        
        
        system.debug('Statusss - '+ Status);
        List<QB_Integration_Settings__c> accesslist = new List<QB_Integration_Settings__c>([Select Access_Token__c, URL__c, Company_ID__c from QB_Integration_Settings__c where Client_ID__c != NULL limit 1]);
        for(QB_Integration_Settings__c qb : accesslist){
            AccessToken = qb.Access_Token__c;
            CompanyId = qb.Company_ID__c;
            URL = qb.URL__c;
        }
        
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeFieldName('Line');
        gen.writeStartArray();
        Integer n = 1;
        for(Invoice_Line_Item__c li :LineItems){
        gen.writeStartObject();
        gen.writeNumberField('Amount', li.Amount__c);
        gen.writeStringField('DetailType', 'SalesItemLineDetail');
        gen.writeFieldName('SalesItemLineDetail');
        gen.writeStartObject();
        gen.writeFieldName('ItemRef');
        gen.writeStartObject();
        gen.writeNumberfield('value',n);
        gen.writeStringfield('name', li.Service__c != NULL ? li.Service__c : '');
        n = n+1;
        gen.writeEndObject();
        gen.writeEndObject();
        gen.writeEndObject();
        }
        gen.writeEndArray();
        gen.writeFieldName('CustomerRef');
        gen.writeStartObject();
        gen.writeStringfield('value',Inv.Account__r.QB_ID__c);
        gen.writeEndObject();  
        

        
        if(Status == 'Create'){
            
            gen.writeEndObject();
            system.debug('Invoice Creation API Called');
            
            String Endpoint = URL + 'v3/company/'+ CompanyId + '/invoice?minorversion=1';
            system.debug('Endpoint -' + Endpoint);
            String responseBody='';
            //gen.writeEndObject();
            System.debug('Json Data: '+gen.getAsString());
            gen.getAsString();
            
            //Send HTTP Request
            Http httpObj = new Http();
            HttpRequest request = new HttpRequest();
            request.setHeader('Accept', 'application/json');
            request.setHeader('Content-Type', 'application/json');
            request.setHeader('Authorization', 'Bearer ' + AccessToken);
            request.setEndpoint(EndPoint);
            request.setMethod('POST');
            request.setBody(gen.getAsString());
            
            try{
                HttpResponse res = httpObj.send(request);
                responseBody = res.getBody();  
                InvoiceJSON body = new InvoiceJSON();
                body = (InvoiceJSON)System.JSON.deserialize(responseBody, InvoiceJSON.class);
                
                system.debug('responseeeee body');
                system.debug(gen.getAsString());
                system.debug(responseBody);
                
                if(body.Invoice.Id != NULL){
                    Inv.QB_ID__c = body.Invoice.ID;
                }
                try{
                    update Inv;
                }
                catch(exception exp){
                    CreateException(exp, request.getBody() + gen.getAsString(), 'Invoice Update Failed - ' + Inv.Id);
                }
            }
            
            catch (exception exp){
                CreateException(exp, request.getBody() + gen.getAsString(), 'Invoice Creation Failed - ' + Inv.Id);
            }
        }
        
        
        if(Status == 'Update'){
            
            String getEndpoint = URL + 'v3/company/'+ CompanyId + '/invoice/' + Inv.QB_ID__c + '?minorversion=1';
            String postEndpoint = URL + 'v3/company/'+ CompanyId + '/invoice?minorversion=1';
            String getresponseBody='';
            String postresponseBody = '';
            
            Http gethttpObj = new Http();
            HttpRequest getrequest = new HttpRequest();
            getrequest.setHeader('Accept', 'application/json');
            getrequest.setHeader('Content-Type', 'application/json');
            getrequest.setHeader('Authorization', 'Bearer ' + AccessToken);
            getrequest.setEndpoint(getEndPoint);
            getrequest.setMethod('GET');
            try{
                HttpResponse getres = gethttpObj.send(getrequest);
                getresponseBody = getres.getBody();  
                InvoiceJSON body = new InvoiceJSON();
                body = (InvoiceJSON)System.JSON.deserialize(getresponseBody, InvoiceJSON.class);
                
                system.debug('GET responseeeee body');
                system.debug(getresponseBody);
                
                if(body.Invoice.SyncToken != NULL){
                    Sync = body.Invoice.SyncToken;
                }
            }
            catch (exception exp){
                CreateException(exp, getrequest.getBody() + getresponseBody , 'Could not fetch Sync Token' + Inv.Id);
            }
            
            gen.writeStringField('Id', Inv.QB_ID__c);
            gen.writeStringField('SyncToken', Sync);
			gen.writeFieldName('CustomerRef');
            gen.writeStartObject();
			gen.writeStringfield('value',Inv.Account__r.QB_ID__c);
			gen.writeStringField('name',Inv.Account__r.Name);
			gen.writeEndObject();
            gen.writeEndObject();
            System.debug('Json Data: '+gen.getAsString());
            gen.getAsString();
            
            //Send HTTP Request
            Http posthttpObj = new Http();
            HttpRequest postrequest = new HttpRequest();
            postrequest.setHeader('Accept', 'application/json');
            postrequest.setHeader('Content-Type', 'application/json');
            postrequest.setHeader('Authorization', 'Bearer ' + AccessToken);
            postrequest.setEndpoint(postEndpoint);
            postrequest.setMethod('POST');
            postrequest.setBody(gen.getAsString());
            
            try{
                HttpResponse postres = posthttpObj.send(postrequest);
                postresponseBody = postres.getBody();  
                InvoiceJSON postbody = new InvoiceJSON();
                postbody = (InvoiceJSON)System.JSON.deserialize(postresponseBody, InvoiceJSON.class);
                
                system.debug('post responseeeee body');
                system.debug(gen.getAsString());
                system.debug(postresponseBody);
                
            }
            
            catch (exception exp){
                CreateException(exp, postrequest.getBody() + gen.getAsString(), 'Invoice Update Failed - ' + Inv.Id);
            }
        }
    }
    
    
    
    public static void CreateException(Exception exp, String Body, String Name){
        Exception__c log = new Exception__c();
        if(exp != NULL)
            log.Exception__c = '**** Exception '+exp.getCause()+' '+exp.getMessage()+' '+exp.getLineNumber();
        log.Body__c = Body;
        log.Name = Name;                 
        insert log;
    }
    
    public class InvoiceJSON{
        public InvoiceDetails Invoice {get;set;}
    }
    
    
    public class InvoiceDetails{
        public string ID{get;set;}
        public string SyncToken{get;set;}
    }
}