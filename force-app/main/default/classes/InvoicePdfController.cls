public class InvoicePdfController {
    
    Id id = ApexPages.currentPage().getParameters().get('Id');
    public Invoice__c invoice {get; set;}
    public Opportunity opportunity {get; set;}
    public Account account {get; set;}
    
    public Map<String, Decimal> lineItemDetails {get; set;}
    public List<Decimal> depositPaid {get; set;}
    
    public String  clientname{get; set;}
    public String  title{get; set;}
    
    public String oppname{get;set;}
    public  String todayDate{get;set;}
    public String terms{get;set;}
    public String location{get;set;}
    DateTime myDateTime;
    public  String duedate{get;set;}
    public String startDate{get;set;}
    public String endDate{get;set;}
    public Decimal lineItemTotal{get;set;}
    public Decimal depositTotal{get;set;}
    public Decimal totalDue{get;set;}
    
    public InvoicePdfController(ApexPages.StandardController standardPageController) {
        
        this.invoice=[SELECT Id, Invoice_Ad__c,Invoice_City__c, Invoice_Country__c,Opportunity__c,Invoice_Date__c,Due_Date__c,Name,Description__c,Invoice_Amount__c,Account__c,Is_it_a_final_Invoice_for_this_Contact__c,
                      Description_1__c, Description_2__c, Description_3__c, Description_4__c, Description_5__c,
                      Description_6__c, Description_7__c, Description_8__c, Description_9__c, Description_10__c,
                      Rate_1__c, Rate_2__c, Rate_3__c, Rate_4__c, Rate_5__c,
                      Rate_6__c, Rate_7__c, Rate_8__c, Rate_9__c, Rate_10__c,
                      Deposit_Paid_1__c, Deposit_Paid_2__c, Deposit_Paid_3__c, Deposit_Paid_4__c, Deposit_Paid_5__c,
                      Deposit_Paid_6__c, Deposit_Paid_7__c, Deposit_Paid_8__c, Deposit_Paid_9__c, Deposit_Paid_10__c,
                      Total_Line_Item_Amounts__c, Total_Deposit_Paid__c,Balance__c,Number_of_Line_Items__c, Number_of_Deposit__c
                      FROM Invoice__c
                      WHERE id =: id
                     ];
        
        if(invoice.Is_it_a_final_Invoice_for_this_Contact__c == 'Yes'){
            lineItemDetails = new Map<String, Decimal>();
            Integer noOfLineItems = integer.valueof(invoice.Number_of_Line_Items__c);
            
            if(!String.isBlank(invoice.Description_1__c) && (invoice.Rate_1__c != null || invoice.Rate_1__c != 0) && noOfLineItems >= 2){
                lineItemDetails.put(invoice.Description_1__c, invoice.Rate_1__c);
            }
            
            if(!String.isBlank(invoice.Description_2__c) && (invoice.Rate_2__c != null || invoice.Rate_2__c != 0) && noOfLineItems >= 2){
                lineItemDetails.put(invoice.Description_2__c, invoice.Rate_2__c);
            }
            
            if(!String.isBlank(invoice.Description_3__c) && (invoice.Rate_3__c != null || invoice.Rate_3__c != 0) && noOfLineItems >= 3){
                lineItemDetails.put(invoice.Description_3__c, invoice.Rate_3__c);
            }
            
            if(!String.isBlank(invoice.Description_4__c) && (invoice.Rate_4__c != null || invoice.Rate_4__c != 0) && noOfLineItems >= 4){
                lineItemDetails.put(invoice.Description_4__c, invoice.Rate_4__c);
            }
            
            if(!String.isBlank(invoice.Description_5__c) && (invoice.Rate_5__c != null || invoice.Rate_5__c != 0) && noOfLineItems >= 5){
                lineItemDetails.put(invoice.Description_5__c, invoice.Rate_5__c);
            }
            
            if(!String.isBlank(invoice.Description_6__c) && (invoice.Rate_6__c != null || invoice.Rate_6__c != 0) && noOfLineItems >= 6){
                lineItemDetails.put(invoice.Description_6__c, invoice.Rate_6__c);
            }
            
            if(!String.isBlank(invoice.Description_7__c) && (invoice.Rate_7__c != null || invoice.Rate_7__c != 0) && noOfLineItems >= 7){
                lineItemDetails.put(invoice.Description_7__c, invoice.Rate_7__c);
            }
            
            if(!String.isBlank(invoice.Description_8__c) && (invoice.Rate_8__c != null || invoice.Rate_8__c != 0) && noOfLineItems >= 8){
                lineItemDetails.put(invoice.Description_8__c, invoice.Rate_8__c);
            }
            
            if(!String.isBlank(invoice.Description_9__c) && (invoice.Rate_9__c != null || invoice.Rate_9__c != 0) && noOfLineItems >= 9){
                lineItemDetails.put(invoice.Description_9__c, invoice.Rate_9__c);
            }
            
            if(!String.isBlank(invoice.Description_10__c) && (invoice.Rate_10__c != null || invoice.Rate_10__c != 0) && noOfLineItems == 10){
                lineItemDetails.put(invoice.Description_10__c, invoice.Rate_10__c);
            }
        }
        
        if(invoice.Is_it_a_final_Invoice_for_this_Contact__c == 'Yes'){
            depositPaid = new List<Decimal>();
            Integer noOfDeposit = integer.valueof(invoice.Number_of_Deposit__c);
            
            if(invoice.Deposit_Paid_1__c != null && invoice.Deposit_Paid_1__c != 0 && noOfDeposit >= 1){
                depositPaid.add(invoice.Deposit_Paid_1__c);
            }
            
            if(invoice.Deposit_Paid_2__c != null && invoice.Deposit_Paid_2__c != 0  && noOfDeposit >= 2){
                depositPaid.add(invoice.Deposit_Paid_2__c);
            }
            
            if(invoice.Deposit_Paid_3__c != null && invoice.Deposit_Paid_3__c != 0  && noOfDeposit >= 3){
                depositPaid.add(invoice.Deposit_Paid_3__c);
            }
            
            if(invoice.Deposit_Paid_4__c != null && invoice.Deposit_Paid_4__c != 0  && noOfDeposit >= 4){
                depositPaid.add(invoice.Deposit_Paid_4__c);
            }
            
            if(invoice.Deposit_Paid_5__c != null && invoice.Deposit_Paid_5__c != 0  && noOfDeposit >= 5){
                depositPaid.add(invoice.Deposit_Paid_5__c);
            }
            
            if(invoice.Deposit_Paid_6__c != null && invoice.Deposit_Paid_6__c != 0  && noOfDeposit >= 6){
                depositPaid.add(invoice.Deposit_Paid_6__c);
            }
            
            if(invoice.Deposit_Paid_7__c != null && invoice.Deposit_Paid_7__c != 0  && noOfDeposit >= 7){
                depositPaid.add(invoice.Deposit_Paid_7__c);
            }
            
            if(invoice.Deposit_Paid_8__c != null && invoice.Deposit_Paid_8__c != 0  && noOfDeposit >= 8){
                depositPaid.add(invoice.Deposit_Paid_8__c);
            }
            
            if(invoice.Deposit_Paid_9__c != null && invoice.Deposit_Paid_9__c != 0  && noOfDeposit >= 9){
                depositPaid.add(invoice.Deposit_Paid_9__c);
            }
            
            if(invoice.Deposit_Paid_10__c != null && invoice.Deposit_Paid_10__c != 0  && noOfDeposit == 10){
                depositPaid.add(invoice.Deposit_Paid_10__c);
            }
        }        
        
        if(invoice.Total_Line_Item_Amounts__c == null){
            lineItemTotal = 0;
        }else{
            lineItemTotal = invoice.Total_Line_Item_Amounts__c;
        }
        
        if(invoice.Total_Deposit_Paid__c == null){
            depositTotal = 0;
        }else{
            depositTotal = invoice.Total_Deposit_Paid__c;
        }
        
        totalDue = lineItemTotal - depositTotal;
        
        if(invoice.Invoice_Date__c != null)
        {
            myDateTime = (DateTime) (invoice.Invoice_Date__c+1);
            todayDate = myDateTime.format('MM/dd/yyyy');
        }
        
        if(invoice.Due_Date__c != null)
        {
            myDateTime = (DateTime) (invoice.Due_Date__c+1);
            duedate = myDateTime.format('MM/dd/yyyy');
        }
        
        opportunity=[SELECT Id,Name,Client_Name1__c,Terms__c, Start_Date__c,End_Date__c,Location_Details__c
                     FROM opportunity
                     WHERE id =:invoice.Opportunity__c ];
        
        oppname=opportunity.Name;
        terms=opportunity.Terms__c;
        location=opportunity.Location_Details__c;
        
        if(opportunity.Start_Date__c != null)
        {
            myDateTime = (DateTime) (opportunity.Start_Date__c);
            startDate = myDateTime.format('MM/dd/yyyy');
        }
        
        if(opportunity.End_Date__c != null)
        {
            myDateTime = (DateTime) (opportunity.End_Date__c);
            endDate = myDateTime.format('MM/dd/yyyy');
        }
        
        Contact contact=[SELECT Id,Name,Title
                         From contact
                         WHERE id =:Opportunity.Client_Name1__c];
        clientname=contact.Name;
        title=contact.Title;
        
        account = [SELECT Id, Name, BillingStreet, BillingCity, BillingState, BillingPostalCode,BillingCountry
                   
                   from Account WHERE Id =:invoice.Account__c ];            
    }
    
    public void generatePDF() {
        
        if(ApexPages.currentPage().getParameters().containsKey('generatepdf')) {
            return;
        }
        
        List<Attachment> attachListToDel = [SELECT Id,ParentId FROM Attachment WHERE ParentId =: id];
        delete attachListToDel;
        
        PageReference pdfPage = ApexPages.currentPage();
        pdfPage.getParameters().put('generatepdf','true');
        
        Attachment attach = new Attachment();
        Blob pdfBlob;
        try {
            pdfBlob = pdfPage.getContent();
        }catch (VisualforceException e) {
            pdfBlob = Blob.valueOf('Error Uploading' + e.getMessage());
        }
        
        attach.parentId = id;
        attach.Name = invoice.Name  + '.pdf';
        attach.body = pdfBlob;
        insert attach;
        
    }
}