public class qb_customercreation implements Queueable, Database.AllowsCallouts{
    
    private ID RecordId;
    private String ObjectType;
    public String AccessToken;
    public String CompanyId;
    public String URL;
    
    public qb_customercreation(Id recId){
        RecordId = recId;
    }
    
    public void execute(QueueableContext context) {       
        Account Acc = [Select Id, 
                       Name,
                       BillingStreet,
                       BillingCity,
                       BillingCountry,
                       BillingState,
                       BillingPostalCode,
                       Description,
                       Phone,
                       Email__c
                       from Account where Id =: RecordId limit 1];        
        
        //String Endpoint = 'https://sandbox-quickbooks.api.intuit.com/v3/company/4620816365239590060/customer?minorversion=1';
        
        system.debug('Customer Creation API Called');
        
        List<QB_Integration_Settings__c> accesslist = new List<QB_Integration_Settings__c>([Select Access_Token__c, URL__c, Company_ID__c from QB_Integration_Settings__c where Client_ID__c != NULL limit 1]);
        for(QB_Integration_Settings__c qb : accesslist){
            AccessToken = qb.Access_Token__c;
            CompanyId = qb.Company_ID__c;
            URL = qb.URL__c;
        }
        String Endpoint = URL + 'v3/company/'+ CompanyId + '/customer?minorversion=1';
        system.debug('Endpoint -' + Endpoint);
        String responseBody='';
        
        
        
        // Generate JSON body
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeFieldName('BillAddr');
        gen.writeStartObject();
        gen.writeStringField('Line1', acc.BillingStreet);
        gen.writeStringField('City', acc.BillingCity);
        gen.writeStringField('Country', acc.BillingCountry);
        gen.writeStringField('CountrySubDivisionCode', acc.BillingState);
        gen.writeStringField('PostalCode', acc.BillingPostalCode);
        gen.writeEndObject();
        gen.writeStringField('DisplayName',acc.Name != NULL ? acc.Name:'');
        gen.writeStringField('Notes',acc.Description != NULL ? acc.Description : '');
        gen.writeFieldName('PrimaryPhone');
        gen.writeStartObject();
        gen.writeStringField('FreeFormNumber', '9740720445');
        gen.writeEndObject();
        gen.writeFieldName('PrimaryEmailAddr');
        gen.writeStartObject();
        gen.writeStringField('Address', 'varunnagpal86@gmail.com');
        gen.writeEndObject();
        
        gen.writeEndObject();
        System.debug('Json Data: '+gen.getAsString());
        gen.getAsString();
        
        
        //Send HTTP Request
        Http httpObj = new Http();
        HttpRequest request = new HttpRequest();
        request.setHeader('Accept', 'application/json');
        request.setHeader('Content-Type', 'application/json');
        request.setHeader('Authorization', 'Bearer ' + AccessToken);
        request.setEndpoint(EndPoint);
        request.setMethod('POST');
        request.setBody(gen.getAsString());
        
        try{
            HttpResponse res = httpObj.send(request);
            responseBody = res.getBody();  
            CustomerJSON body = new CustomerJSON();
            body = (CustomerJSON)System.JSON.deserialize(responseBody, CustomerJSON.class);
            
            system.debug('responseeeee body');
            system.debug(gen.getAsString());
            system.debug(responseBody);
            
            if(body.Customer.Id != NULL){
                acc.QB_ID__c = body.Customer.Id;
            }
            try{
                update acc;
            }
            catch(exception exp){
                CreateException(exp, request.getBody() + gen.getAsString(), 'Customer Update Failed - ' + acc.Id);
            }
        }
        
        catch (exception exp){
            CreateException(exp, request.getBody() + gen.getAsString(), 'Customer Creation Failed - ' + acc.Id);
        }
    }
    
    public void getcustomer(QueueableContext context) {  
        
    }
    
    public static void CreateException(Exception exp, String Body, String Name){
        Exception__c log = new Exception__c();
        if(exp != NULL)
            log.Exception__c = '**** Exception '+exp.getCause()+' '+exp.getMessage()+' '+exp.getLineNumber();
        log.Body__c = Body;
        log.Name = Name;                 
        insert log;
    }
    
    public class CustomerJSON{
        public CustomerDetails Customer {get;set;}
    }
    
    
    public class CustomerDetails{
        public string Id{get;set;}
    }
    
}