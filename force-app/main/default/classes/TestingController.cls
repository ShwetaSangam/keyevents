public class TestingController {
    
    
    
    Id id = ApexPages.currentPage().getParameters().get('Id');
    public Opportunity opportunity {get; set;}
    
    public User user {get; set;}
    public String OpportunityId; 
    public Contact contact{get;set;}
    DateTime myDateTime;
    public Integer dayOfWeek {get; set;}
    public String  month{get; set;}
    public Integer  year{get; set;}
    Public String companyName{get;set;}
    public String todayDate{get;set;}
    public String scheduleDate{get;set;}
    public String clientname{get;set;}
    public String phonenumber{get;set;}
    public String email{get;set;}
    public String startdate{get;set;}
    public String enddate{get;set;}
    
    
    public TestingController(ApexPages.StandardController standardPageController) {
        OpportunityId = ApexPages.currentPage().getParameters().get('Id' ); 
        
        this.opportunity = [SELECT Id,Name,Event_Date__c, Update_Percentage_Values__c, Account.name,Commencement_of_Agreement_Date__c,Client_Name1__r.name,Contact_Email__c,
                            Client_Address__c,Deposit_Schedule_First__c,Deposit_Schedule_Second__c,Deposit_Schedule_Due_Date__c,EXHIBIT_A__c,Start_Date__c,End_Date__c,
                            Version__c,OwnerId, Client_Name1__c, X30_Days_Prior_Deposit__c 	
                            FROM Opportunity
                            WHERE Id =: OpportunityId
                           ];
        
        this.user = [SELECT Name,Email from User where id =:opportunity.OwnerId];
        
        companyName = opportunity.Account.name;
        
        if(opportunity.Client_Name1__c != null || opportunity.Client_Name1__c != ' '){
            contact=[SELECT Id,Name,Title,Phone,MailingStreet, MailingCity, MailingState, MailingPostalCode,MailingCountry,Email
                     From contact
                     WHERE id =:opportunity.Client_Name1__c];
        }       
        
        if(opportunity.Commencement_of_Agreement_Date__c != null)
        {
            myDateTime = (DateTime) (opportunity.Commencement_of_Agreement_Date__c+1);
            todayDate = myDateTime.format('MM/dd/yyyy');
        }
        
        if(opportunity.Start_Date__c!= null)
        {
            myDateTime = (DateTime) (opportunity.Start_Date__c+1);
            startdate = myDateTime.format('MM/dd/yyyy');
        }        
        
        if(opportunity.End_Date__c!= null)
        {
            myDateTime = (DateTime) (opportunity.End_Date__c+1);
            enddate = myDateTime.format('MM/dd/yyyy');
        }   
        
        if(opportunity.Deposit_Schedule_Due_Date__c != null)
        {
            myDateTime = (DateTime) (opportunity.Deposit_Schedule_Due_Date__c+1);
            scheduleDate =myDateTime.format('MM/dd/yyyy');  
        }
        
        if(opportunity.Event_Date__c != null)
        {
            myDateTime = (DateTime) opportunity.Event_Date__c;
            year = myDateTime.year();
            dayOfWeek = myDateTime.day()+1;
            month = myDateTime.format('MMMM');  
        }
    }
    
    /*public void generatePDF() {
        
        if(ApexPages.currentPage().getParameters().containsKey('generatepdf')) {
            return;
        }
        
        List<Attachment> attachListToDel = [SELECT Id,ParentId FROM Attachment WHERE ParentId =: id];
        delete attachListToDel;
        
        PageReference pdfPage = ApexPages.currentPage();
        //pdfPage.setRedirect(true);
        pdfPage.getParameters().put('generatepdf','true');
        
        Attachment attach = new Attachment();
        Blob pdfBlob;
        try {
            pdfBlob = pdfPage.getContent();
        }catch (VisualforceException e) {
            pdfBlob = Blob.valueOf('Error Uploading' + e.getMessage());
        }
        
        attach.parentId = id;
        attach.Name = opportunity.Name  + '.pdf';
        attach.body = pdfBlob;
        insert attach;
        
    }*/
    
    public PageReference generatePDF() {
       PageReference p = new PageReference('/apex/Contract_VF_page?id=' + id);
       p.setRedirect(true);
		return p;
       
    }
    


}