public class qb_product_create implements Queueable, Database.AllowsCallouts{
    
    private ID RecordId;
    private String ObjectType;
    public String AccessToken;
    public String CompanyId;
    public String URL;
    public String Sync;
    public String Status;
    
    public qb_product_create(Id recId, String Stats){
        RecordId = recId;
        Status = Stats;
    }
    
    public void execute(QueueableContext context) {       
        Product2 Prod = [Select Id, 
                       Name,
                       QB_ID__c,
                       Product_Type__c,
                         Quantity_on_Hand__c,
                         Standard_Price__c
                       from Product2 where Id =: RecordId limit 1];        
        
        
        system.debug('Statusss - '+ Status);
        List<QB_Integration_Settings__c> accesslist = new List<QB_Integration_Settings__c>([Select Access_Token__c, URL__c, Company_ID__c from QB_Integration_Settings__c where Client_ID__c != NULL limit 1]);
        for(QB_Integration_Settings__c qb : accesslist){
            AccessToken = qb.Access_Token__c;
            CompanyId = qb.Company_ID__c;
            URL = qb.URL__c;
        }
        
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField('Name', Prod.Name != NULL ? Prod.Name : '');
        gen.writeStringField('Type', Prod.Product_Type__c != NULL ? Prod.Product_Type__c : '');
        gen.writeNumberField('QtyOnHand', Prod.Quantity_on_Hand__c != NULL ? Prod.Quantity_on_Hand__c : 0);
        gen.writeBooleanField('TrackQtyOnHand',true);
        
        
        if(Status == 'Create'){
            
            system.debug('Item/Product Creation API Called');
            
            String Endpoint = URL + 'v3/company/'+ CompanyId + '/item?minorversion=1';
            system.debug('Endpoint -' + Endpoint);
            String responseBody='';
            gen.writeEndObject();
            System.debug('Json Data: '+gen.getAsString());
            gen.getAsString();
            
            //Send HTTP Request
            Http httpObj = new Http();
            HttpRequest request = new HttpRequest();
            request.setHeader('Accept', 'application/json');
            request.setHeader('Content-Type', 'application/json');
            request.setHeader('Authorization', 'Bearer ' + AccessToken);
            request.setEndpoint(EndPoint);
            request.setMethod('POST');
            request.setBody(gen.getAsString());
            
            try{
                HttpResponse res = httpObj.send(request);
                responseBody = res.getBody();  
                ItemJSON body = new ItemJSON();
                body = (ItemJSON)System.JSON.deserialize(responseBody, ItemJSON.class);
                
                system.debug('responseeeee body');
                system.debug(gen.getAsString());
                system.debug(responseBody);
                
                if(body.Item.Id != NULL){
                    Prod.QB_ID__c = body.Item.ID;
                }
                try{
                    update Prod;
                }
                catch(exception exp){
                    CreateException(exp, request.getBody() + gen.getAsString(), 'Product Update Failed - ' + Prod.Id);
                }
            }
            
            catch (exception exp){
                CreateException(exp, request.getBody() + gen.getAsString(), 'Product Creation Failed - ' + Prod.Id);
            }
        }
        
        
       /* if(Status == 'Update'){
            
            String getEndpoint = URL + 'v3/company/'+ CompanyId + '/customer/' + Acc.QB_ID__c + '?minorversion=1';
            String postEndpoint = URL + 'v3/company/'+ CompanyId + '/customer?minorversion=1';
            String getresponseBody='';
            String postresponseBody = '';
            
            Http gethttpObj = new Http();
            HttpRequest getrequest = new HttpRequest();
            getrequest.setHeader('Accept', 'application/json');
            getrequest.setHeader('Content-Type', 'application/json');
            getrequest.setHeader('Authorization', 'Bearer ' + AccessToken);
            getrequest.setEndpoint(getEndPoint);
            getrequest.setMethod('GET');
            try{
                HttpResponse getres = gethttpObj.send(getrequest);
                getresponseBody = getres.getBody();  
                CustomerJSON body = new CustomerJSON();
                body = (CustomerJSON)System.JSON.deserialize(getresponseBody, CustomerJSON.class);
                
                system.debug('GET responseeeee body');
                system.debug(getresponseBody);
                
                if(body.Customer.SyncToken != NULL){
                    Sync = body.Customer.SyncToken;
                }
            }
            catch (exception exp){
                CreateException(exp, getrequest.getBody() + getresponseBody , 'Could not fetch Sync Token' + acc.Id);
            }
            
            gen.writeStringField('Id', acc.QB_ID__c);
            gen.writeStringField('SyncToken', Sync);
            gen.writeEndObject();
            System.debug('Json Data: '+gen.getAsString());
            gen.getAsString();
            
            //Send HTTP Request
            Http posthttpObj = new Http();
            HttpRequest postrequest = new HttpRequest();
            postrequest.setHeader('Accept', 'application/json');
            postrequest.setHeader('Content-Type', 'application/json');
            postrequest.setHeader('Authorization', 'Bearer ' + AccessToken);
            postrequest.setEndpoint(postEndpoint);
            postrequest.setMethod('POST');
            postrequest.setBody(gen.getAsString());
            
            try{
                HttpResponse postres = posthttpObj.send(postrequest);
                postresponseBody = postres.getBody();  
                CustomerJSON postbody = new CustomerJSON();
                postbody = (CustomerJSON)System.JSON.deserialize(postresponseBody, CustomerJSON.class);
                
                system.debug('post responseeeee body');
                system.debug(gen.getAsString());
                system.debug(postresponseBody);
                
            }
            
            catch (exception exp){
                CreateException(exp, postrequest.getBody() + gen.getAsString(), 'Customer Update Failed - ' + acc.Id);
            }
        }*/
    }
    
    
    
    public static void CreateException(Exception exp, String Body, String Name){
        Exception__c log = new Exception__c();
        if(exp != NULL)
            log.Exception__c = '**** Exception '+exp.getCause()+' '+exp.getMessage()+' '+exp.getLineNumber();
        log.Body__c = Body;
        log.Name = Name;                 
        insert log;
    }
    
    public class ItemJSON{
        public ItemDetails Item {get;set;}
    }
    
    
    public class ItemDetails{
        public string ID{get;set;}
        public string SyncToken{get;set;}
    }
}