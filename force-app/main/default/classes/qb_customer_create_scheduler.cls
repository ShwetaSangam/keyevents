global class qb_customer_create_scheduler implements Schedulable{
    global void execute(SchedulableContext sc){
        Database.executeBatch(new qb_customer_create_batch(), 1);       
    }
}