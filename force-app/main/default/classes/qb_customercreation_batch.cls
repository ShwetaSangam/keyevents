global class qb_customercreation_batch implements Database.Batchable <SObject>{
    
    global Database.QueryLocator start (Database.BatchableContext bc){
        return Database.getQueryLocator([Select Id, Name from Account where QB_ID__c = NULL]);
    }
    
    global void execute(Database.BatchableContext bc, List<Account> AccList){
        if(AccList.size()>0){
            for(Account Acc :AccList){
                qb_customercreation newb = new qb_customercreation(Acc.Id);
                ID jobID = System.enqueueJob(newb);
            }
            
        }
    }
    
    global void finish(Database.BatchableContext bc){
        
    }
}