public class QuickbooksIntController {
    
    @AuraEnabled
    public static Boolean saveKeyAndSecret(String clientId, String clientSecret){
        try {
            QB_Integration_Settings__c qb_settings = [Select Client_ID__c, Client_Secret__c from QB_Integration_Settings__c limit 1];
            qb_settings.Client_ID__c= clientId;             
            qb_settings.Client_Secret__c= clientSecret;
            update qb_settings;          
            return true;   
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    
    @AuraEnabled
    public static Boolean getAuthDone(String redirect_URI, String authCodeFromURL, String company_ID){
        try {
            String accessToken_Endpoint = 'https://oauth.platform.intuit.com/oauth2/v1/tokens/bearer?';
            QB_Integration_Settings__c qb_settings = [Select Client_ID__c, Client_Secret__c, Company_ID__c from QB_Integration_Settings__c limit 1];
            
            System.debug(qb_settings);
            System.debug(authCodeFromURL);
            
            Http http = new Http();
            HttpRequest httpReq = new HttpRequest();
            HttpResponse httpRes = new HttpResponse();
            String requestTokenBody = 'code='+authCodeFromURL+'&grant_type=authorization_code'+
                '&client_id='+qb_settings.Client_ID__c+'&client_secret='+qb_settings.Client_Secret__c +
                '&redirect_uri='+redirect_URI;
            
            System.debug('#### authCodeFromURL '+authCodeFromURL);
            System.debug('#### requestTokenBody '+requestTokenBody);
            
            httpReq.setMethod('POST');
            httpReq.setEndpoint(accessToken_Endpoint);
            httpReq.setHeader('Content-Type', 'application/x-www-form-urlencoded');
            httpReq.setBody(requestTokenBody);
            httpRes = http.send(httpReq);
            if(httpRes.getStatusCode() == 200){
                Map<String,object> TokenInfo = (Map<String,object>)JSON.deserializeUntyped(httpRes.getBody());
                qb_settings.Refresh_Token__c = String.valueOf(TokenInfo.get('refresh_token'));
                qb_settings.Access_Token__c = String.valueOf(TokenInfo.get('access_token'));
                qb_settings.Access_Token_Expiry_Date__c = System.now().addSeconds(Integer.valueOf(TokenInfo.get('expires_in')));
                qb_settings.Refresh_Token_Expiry_Date__c = System.now().addSeconds(Integer.valueOf(TokenInfo.get('x_refresh_token_expires_in')));
                qb_settings.Company_ID__c = company_ID;
                update qb_settings;                   
                return true;
            }else{
                return false;
            }
        }
        catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
            
        }
    }
}