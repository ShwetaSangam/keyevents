public class qb_customerupdate implements Queueable, Database.AllowsCallouts{
    
    private ID RecordId;
    private String ObjectType;
    public String AccessToken;
    public String CompanyId;
    public String URL;
    public String Sync;
    
    public qb_customerupdate(Id recId){
        RecordId = recId;
    }
    
    public void execute(QueueableContext context) {       
        Account Acc = [Select Id, 
                       Name,
                       BillingStreet,
                       BillingCity,
                       BillingCountry,
                       BillingState,
                       BillingPostalCode,
                       Description,
                       Phone,
                       Email__c,
                       QB_ID__c
                       from Account where Id =: RecordId limit 1];        
        
        //String Endpoint = 'https://sandbox-quickbooks.api.intuit.com/v3/company/4620816365239590060/customer?minorversion=1';
        
        system.debug('Customer Update API Called');
        
        List<QB_Integration_Settings__c> accesslist = new List<QB_Integration_Settings__c>([Select Access_Token__c, URL__c, Company_ID__c from QB_Integration_Settings__c where Client_ID__c != NULL limit 1]);
        for(QB_Integration_Settings__c qb : accesslist){
            AccessToken = qb.Access_Token__c;
            CompanyId = qb.Company_ID__c;
            URL = qb.URL__c;
        }
        String Endpoint = URL + 'v3/company/'+ CompanyId + '/customer/' + Acc.QB_ID__c + '?minorversion=1';
        String postEndpoint = URL + 'v3/company/'+ CompanyId + '/customer?minorversion=1';
        system.debug('Endpoint -' + Endpoint);
        String getresponseBody='';
        String postresponseBody = '';
        
        Http httpObj = new Http();
        HttpRequest request = new HttpRequest();
        request.setHeader('Accept', 'application/json');
        request.setHeader('Content-Type', 'application/json');
        request.setHeader('Authorization', 'Bearer ' + AccessToken);
        request.setEndpoint(EndPoint);
        request.setMethod('GET');
        try{
        HttpResponse res = httpObj.send(request);
            getresponseBody = res.getBody();  
            CustomerJSON body = new CustomerJSON();
            body = (CustomerJSON)System.JSON.deserialize(getresponseBody, CustomerJSON.class);
            
            system.debug('GET responseeeee body');
            system.debug(getresponseBody);
            
            if(body.Customer.SyncToken != NULL){
               Sync = body.Customer.SyncToken;
            }
        }
        catch (exception exp){
            CreateException(exp, request.getBody() + getresponseBody , 'Could not fetch Sync Token' + acc.Id);
        }
        
        
        // Generate JSON body
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField('Id', acc.QB_ID__c);
        gen.writeFieldName('BillAddr');
        gen.writeStartObject();
        gen.writeStringField('Line1', acc.BillingStreet);
        gen.writeStringField('City', acc.BillingCity);
        gen.writeStringField('Country', acc.BillingCountry);
        gen.writeStringField('CountrySubDivisionCode', acc.BillingState);
        gen.writeStringField('PostalCode', acc.BillingPostalCode);
        gen.writeEndObject();
        gen.writeStringField('DisplayName',acc.Name != NULL ? acc.Name:'');
        gen.writeStringField('Notes',acc.Description != NULL ? acc.Description : '');
        gen.writeFieldName('PrimaryPhone');
        gen.writeStartObject();
        gen.writeStringField('FreeFormNumber',acc.Phone != NULL ? acc.Phone : '');
        gen.writeEndObject();
        gen.writeFieldName('PrimaryEmailAddr');
        gen.writeStartObject();
        gen.writeStringField('Address', acc.Email__c != NULL ? acc.Email__c : '' );
        gen.writeEndObject();
        gen.writeStringField('SyncToken', Sync);
        gen.writeEndObject();
        System.debug('Json Data: '+gen.getAsString());
        gen.getAsString();
        
        
        //Send HTTP Request
        Http posthttpObj = new Http();
        HttpRequest postrequest = new HttpRequest();
        request.setHeader('Accept', 'application/json');
        request.setHeader('Content-Type', 'application/json');
        request.setHeader('Authorization', 'Bearer ' + AccessToken);
        request.setEndpoint(EndPoint);
        request.setMethod('POST');
        request.setBody(gen.getAsString());
        
        try{
            HttpResponse postres = posthttpObj.send(request);
            postresponseBody = postres.getBody();  
            CustomerJSON postbody = new CustomerJSON();
            postbody = (CustomerJSON)System.JSON.deserialize(postresponseBody, CustomerJSON.class);
            
            system.debug('responseeeee body');
            system.debug(gen.getAsString());
            system.debug(postresponseBody);
            
        }
        
        catch (exception exp){
            CreateException(exp, request.getBody() + gen.getAsString(), 'Customer Update Failed - ' + acc.Id);
        }
    }
    
    
    
    public static void CreateException(Exception exp, String Body, String Name){
        Exception__c log = new Exception__c();
        if(exp != NULL)
            log.Exception__c = '**** Exception '+exp.getCause()+' '+exp.getMessage()+' '+exp.getLineNumber();
        log.Body__c = Body;
        log.Name = Name;                 
        insert log;
    }
    
    public class CustomerJSON{
        public CustomerDetails Customer {get;set;}
    }
    
    
    public class CustomerDetails{
        public string SyncToken{get;set;}
        public String ID{get;set;}
    }
    
}