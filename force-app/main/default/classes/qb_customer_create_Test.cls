@isTest
public class qb_customer_create_Test {
    
    public static testMethod void insertTestPositive(){
        Account Acc = new Account();
        Acc.Name = 'TestAccount';
        Acc.Email__c = 'testmail@email.com';
        Acc.Phone = '0000000022';
        Acc.BillingCity = 'City'; 
        Acc.BillingStreet = 'Street';
        Acc.BillingState = 'California';
        Acc.BillingCountry = 'United States';
        Acc.BillingPostalCode = '12345';
        Acc.Description = 'Test Account';
        insert Acc;
        
        Test.startTest();
        qbMockCallout positiveResponse = new qbMockCallout('{"Customer": {"Taxable": true,"BillAddr": {"Id": "98","Line1": "Street","City": "City","Country": "Umited States","CountrySubDivisionCode": "CA","PostalCode": "12345"},"Notes": "Description.","Test Account": false,"BillWithParent": false,"Balance": 0,"BalanceWithJobs": 0,"CurrencyRef": { "value": "USD","name": "United States Dollar"},"PreferredDeliveryMethod": "Print","domain": "QBO","sparse": false,"Id": "60","SyncToken": "0","MetaData": {"CreateTime": "2022-09-01T10:43:23-07:00","LastUpdatedTime": "2022-09-01T10:43:23-07:00"},"FullyQualifiedName": "Test Account","DisplayName": "Test Account","PrintOnCheckName": "Test Account","Active": true,"PrimaryPhone": {"FreeFormNumber": "(000) 000-0022"},"PrimaryEmailAddr": {"Address": "testmail@email.com"},"DefaultTaxCodeRef": {"value": "2"}},"time": "2022-09-01T10:43:22.968-07:00"}'); 
        Test.setMock(HttpCalloutMock.class, positiveResponse);
        qb_customer_create updatejob = new qb_customer_create(Acc.Id,'Create');
        ID jobID = System.enqueueJob(updateJob); 
        Test.stopTest();
        
        //Test the flow action
        qb_customer_create_action.inputValues inputValues = new qb_customer_create_action.inputValues();
        inputValues.accID = Acc.Id;
        
        qb_customer_create_action.inputValues[] iValues = new qb_customer_create_action.inputValues[]{};
            iValues.add(inputValues);
        qb_customer_create_action.createCustomer(iValues);
        
    }
    
    public static testMethod void insertTestNegative(){
        Account Acc = new Account();
        Acc.Name = 'TestAccount';
        Acc.Email__c = 'testmail@email.com';
        Acc.Phone = '0000000022';
        Acc.BillingCity = 'City';
        Acc.BillingStreet = 'Street';
        Acc.BillingState = 'California';
        Acc.BillingCountry = 'United States';
        Acc.BillingPostalCode = '12345';
        Acc.Description = 'Test Account';
        insert Acc;
        
        Test.startTest();
        qbMockCallout positiveResponse = new qbMockCallout('{"Customer": {"Taxable": true,"BillAddr": {"Id":""}}}'); 
        Test.setMock(HttpCalloutMock.class, positiveResponse);
        qb_customer_create updatejob = new qb_customer_create(Acc.Id,'Create');
        ID jobID = System.enqueueJob(updateJob); 
        Test.stopTest();
    }
    
    public static testMethod void scheduler(){
        Account Acc = new Account();
        Acc.Name = 'TestAccount';
        Acc.Email__c = 'testmail@email.com';
        Acc.Phone = '0000000022';
        Acc.BillingCity = 'City'; 
        Acc.BillingStreet = 'Street';
        Acc.BillingState = 'California';
        Acc.BillingCountry = 'United States';
        Acc.BillingPostalCode = '12345';
        Acc.Description = 'Test Account';
        insert Acc;
        
        Test.startTest();
        qb_customer_create_scheduler Scheduler = new qb_customer_create_scheduler();
        String sch = '0 0 23 * * ?'; 
        System.schedule('Customer Sceduler', sch, Scheduler); 
        Test.stopTest();
    }
    
    /*public static testMethod void testupdate(){
        Account Acc = new Account();
        Acc.Name = 'TestAccount';
        Acc.Email__c = 'testmail@email.com';
        Acc.Phone = '0000000022';
        Acc.BillingCity = 'City'; 
        Acc.BillingStreet = 'Street';
        Acc.BillingState = 'California';
        Acc.BillingCountry = 'United States';
        Acc.BillingPostalCode = '12345';
        Acc.Description = 'Test Account';
        Acc.QB_ID__c = '24';
        String Sync = '0';
        insert Acc;
        
        Test.startTest();
        
        //qbMockCallout getResponse = new qbMockCallout('{"Customer": {"Id": "24","SyncToken":"0"}'); 
        //Test.setMock(HttpCalloutMock.class, getResponse);
        qb_customer_create updatejob = new qb_customer_create(Acc.Id,'Update');
        qbMockCallout getResponse = new qbMockCallout('{"Customer": {"Id": "24","SyncToken":"0"}'); 
        Test.setMock(HttpCalloutMock.class, getResponse);
        qbMockCallout postResponse = new qbMockCallout('{"Customer": {"Id": "24","SyncToken":"0"}'); 
        Test.setMock(HttpCalloutMock.class, postResponse);
        ID jobID = System.enqueueJob(updateJob); 
        Test.stopTest();
        
        //Test the flow action
        qb_customer_create_action.inputValues inputValues = new qb_customer_create_action.inputValues();
        inputValues.accID = Acc.Id;
        
        qb_customer_create_action.inputValues[] iValues = new qb_customer_create_action.inputValues[]{};
            iValues.add(inputValues);
        qb_customer_create_action.createCustomer(iValues);
        
    }*/
}