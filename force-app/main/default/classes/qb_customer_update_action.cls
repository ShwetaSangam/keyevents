public class qb_customer_update_action {
    
    public class inputValues{
        @InvocableVariable(label='Account ID' required=true)
        public string accID;
    }
    
    @InvocableMethod(label='Update Customer in QB') 
    public static void updateCustomer(List<inputValues> inputs){
        
        set<ID> appIDs = new set<ID>();
        
        for(inputValues i : inputs) {
            qb_customer_update updateJob = new qb_customer_update(i.accID);
            ID jobID = System.enqueueJob(updateJob); 
        }  
    }
}