global class qb_invoice_create_batch implements Database.Batchable <SObject>{
    
    global Database.QueryLocator start (Database.BatchableContext bc){
        return Database.getQueryLocator([Select Id, Name from Invoice__c where QB_ID__c = NULL AND Account__r.QB_ID__c != NULL]);
    }
    
    global void execute(Database.BatchableContext bc, List<Invoice__c> InvList){
        if(InvList.size()>0){
            for(Invoice__c Inv :InvList){
                qb_invoice_create newb = new qb_invoice_create(Inv.Id, 'Create');
                ID jobID = System.enqueueJob(newb);
            }
            
        }
    }
    
    global void finish(Database.BatchableContext bc){
        
    }
}