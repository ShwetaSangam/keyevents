public class qb_customer_create_action {
    
    public class inputValues{
        @InvocableVariable(label='Account ID' required=true)
        public string accID;
        @InvocableVariable( label = 'Status' required = true)
        public String Status;
    }
    
    @InvocableMethod(label='Create Customer in QB') 
    public static void createCustomer(List<inputValues> inputs){
        
        set<ID> appIDs = new set<ID>();
        
        for(inputValues i : inputs) {
            qb_customer_create updateJob = new qb_customer_create(i.accID, i.Status);
            ID jobID = System.enqueueJob(updateJob); 
        }  
    }
}