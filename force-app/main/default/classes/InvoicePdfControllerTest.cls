@isTest
public class InvoicePdfControllerTest {
    
    static testMethod void testMethod1() 
    {
        Account testAccount = new Account();
        testAccount.Name = 'Test Account';
        testAccount.Type = 'Prospect';
        testAccount.Industry = 'Automative';
        insert testAccount;
        
        Contact con = new Contact();
        con.LastName='Raktim1';
        
        con.AccountId=testAccount.Id;
        insert con;
        
        Opportunity testOpportunity = new Opportunity();
        testOpportunity.Name = 'Testing';
        testOpportunity.AccountId = testAccount.Id;
        testOpportunity.CloseDate = System.today();
        testOpportunity.type = 'Existing Business';
        testOpportunity.Client_Name1__c = con.Id;
        testOpportunity.StageName='Discovery';
        testOpportunity.LeadSource='Customer Referral';
        testOpportunity.Commencement_of_Agreement_Date__c=System.today();
        testOpportunity.Start_Date__c=System.today();
        testOpportunity.Event_Date__c = System.today();
        testOpportunity.Deposit_Schedule_Due_Date__c = System.today();
        testOpportunity.End_Date__c = System.today();
        insert testOpportunity;
        
        Invoice__c testInvoice = new Invoice__c();
        testInvoice.Account__c = testAccount.Id;
        testInvoice.Opportunity__c = testOpportunity.Id;
        testInvoice.Is_it_a_final_Invoice_for_this_Contact__c = 'yes';
        testInvoice.Description_1__c = 'Helllo there1';
        testInvoice.Description_2__c = 'Helllo there2';
        testInvoice.Description_3__c = 'Helllo there3';
        testInvoice.Description_4__c = 'Helllo there4';
        testInvoice.Description_5__c = 'Helllo there5';
        testInvoice.Description_6__c = 'Helllo there6';
        testInvoice.Description_7__c = 'Helllo there7';
        testInvoice.Description_8__c = 'Helllo there8';
        testInvoice.Description_9__c = 'Helllo there9';
        testInvoice.Description_10__c = 'Helllo there10';
        
        testInvoice.Rate_1__c = 345;
        testInvoice.Rate_2__c = 345;
        testInvoice.Rate_3__c = 345;
        testInvoice.Rate_4__c = 345;
        testInvoice.Rate_5__c = 345;
        testInvoice.Rate_6__c = 345;
        testInvoice.Rate_7__c = 345;
        testInvoice.Rate_8__c = 345;
        testInvoice.Rate_9__c = 345;
        testInvoice.Rate_10__c = 345;
        
        testInvoice.Deposit_Paid_1__c = 400;
        testInvoice.Deposit_Paid_2__c = 400;
        testInvoice.Deposit_Paid_3__c = 400;
        testInvoice.Deposit_Paid_4__c = 400;
        testInvoice.Deposit_Paid_5__c = 400;
        testInvoice.Deposit_Paid_6__c = 400;
        testInvoice.Deposit_Paid_7__c = 400;
        testInvoice.Deposit_Paid_8__c = 400;
        testInvoice.Deposit_Paid_9__c = 400;
        testInvoice.Deposit_Paid_10__c = 400;
        
        testInvoice.Total_Deposit_Paid__c = 2000;
        testInvoice.Total_Line_Item_Amounts__c = 1725; 
        
        testInvoice.Number_of_Deposit__c = '10';
        testInvoice.Number_of_Line_Items__c = '10';        
        
        insert testInvoice;
        
        Invoice__c testInvoice2 = new Invoice__c();
        testInvoice2.Account__c = testAccount.Id;
        testInvoice2.Opportunity__c = testOpportunity.Id;
        testInvoice2.Invoice_Date__c = System.today();
        testInvoice2.Due_Date__c = System.today();
        
        insert testInvoice2;
        
        Test.StartTest(); 
        ApexPages.StandardController sc = new ApexPages.StandardController(testInvoice);
        PageReference pageRef = Page.InvoicePdf;
        Test.setCurrentPage(pageRef);
        
        ApexPages.currentPage().getParameters().put('id', String.valueOf(testInvoice.Id));
        
        InvoicePdfController controller = new InvoicePdfController(sc);
        
        controller.generatePDF();
        
        sc = new ApexPages.StandardController(testInvoice2);
        ApexPages.currentPage().getParameters().put('id', String.valueOf(testInvoice2.Id));
        
        controller = new InvoicePdfController(sc);
        controller.generatePDF();
        
        Test.StopTest();
        
    }
}