public class qb_customercreation_action {
    
    public class inputValues{
        @InvocableVariable(label='Account ID' required=true)
        public string accID;
    }
    
    @InvocableMethod(label='Create Customer in QB') 
    public static void createCustomer(List<inputValues> inputs){
        
        set<ID> appIDs = new set<ID>();
        
        for(inputValues i : inputs) {
            qb_customercreation updateJob = new qb_customercreation(i.accID);
            ID jobID = System.enqueueJob(updateJob); 
        }  
    }
}