public class ContractPdfController {
    
    Id id = ApexPages.currentPage().getParameters().get('Id');
    public Opportunity opportunity {get; set;}
    
    public User user {get; set;}
    public String OpportunityId; 
    public Contact contact{get;set;}
    DateTime myDateTime;
    public Integer dayOfWeek {get; set;}
    public String  month{get; set;}
    public Integer  year{get; set;}
    Public String companyName{get;set;}
    public String todayDate{get;set;}
    public String scheduleDate{get;set;}
    public String clientname{get;set;}
    public String phonenumber{get;set;}
    public String email{get;set;}
    public String startdate{get;set;}
    public String enddate{get;set;}
    public Map<String, Map<Decimal, Date>> scheduleData{get;set;}
    public Integer numberofinstallment{get;set;}
    
     
    
    
    public ContractPdfController(ApexPages.StandardController standardPageController) {
        OpportunityId = ApexPages.currentPage().getParameters().get('Id' ); 
        
        this.opportunity = [SELECT Id,Name,Event_Date__c, Update_Percentage_Values__c, Account.name,Commencement_of_Agreement_Date__c,Client_Name1__r.name,Contact_Email__c,
                            Client_Address__c,Deposit_Schedule_Due_Date__c,EXHIBIT_A__c,Start_Date__c,End_Date__c,
                            Version__c,OwnerId, Client_Name1__c, X30_Days_Prior_Deposit__c 	, Payment_Term__c,
                            Deposit_Schedule_First__c, Deposit_Schedule_Second__c, Third_Deposit_Schedule__c, Fourth_Deposit_Schedule__c, Fifth_Deposit_Schedule__c,
                            Sixth_Deposit_Schedule__c, Seventh_Deposit_Schedule__c, Eighth_Deposit_Schedule__c, Ninth_Deposit_Schedule__c, Tenth_Deposit_Schedule__c,
                            Eleventh_Deposit_Schedule__c, Twelfth_Deposit_Schedule__c,
                            First_Schedule_Due_Date__c, Second_Schedule_Due_Date__c, Third_Schedule_Due_Date__c, Fourth_Schedule_Due_Date__c, Fifth_Schedule_Due_Date__c,
                            Sixth_Schedule_Due_Date__c, Seventh_Schedule_Due_Date__c, Eighth_Schedule_Due_Date__c, Ninth_Schedule_Due_Date__c, Tenth_Schedule_Due_Date__c,
                            Eleventh_Schedule_Due_Date__c, Twelfth_Schedule_Due_Date__c
                            FROM Opportunity
                            WHERE Id =: OpportunityId
                           ];
        
        
        numberofinstallment = 0;
        if(opportunity.Payment_Term__c != null )
        {
           numberofinstallment =Integer.valueOf(opportunity.Payment_Term__c);
        }
        
     
        
        this.user = [SELECT Name,Email from User where id =:opportunity.OwnerId];
        
        companyName = opportunity.Account.name;
        
        if(opportunity.Client_Name1__c != null || opportunity.Client_Name1__c != ' ')
        {
            contact=[SELECT Id,Name,Title,Phone,MailingStreet, MailingCity, MailingState, MailingPostalCode,MailingCountry,Email
                     From contact
                     WHERE id =:opportunity.Client_Name1__c];
        }       
        
        if(opportunity.Commencement_of_Agreement_Date__c != null)
        {
            myDateTime = (DateTime) (opportunity.Commencement_of_Agreement_Date__c+1);
            todayDate = myDateTime.format('MM/dd/yyyy');
        }
        
        if(opportunity.Start_Date__c!= null)
        {
            myDateTime = (DateTime) (opportunity.Start_Date__c+1);
            startdate = myDateTime.format('MM/dd/yyyy');
        }        
        
        if(opportunity.End_Date__c!= null)
        {
            myDateTime = (DateTime) (opportunity.End_Date__c+1);
            enddate = myDateTime.format('MM/dd/yyyy');
        }   
        
        if(opportunity.Deposit_Schedule_Due_Date__c != null)
        {
            myDateTime = (DateTime) (opportunity.Deposit_Schedule_Due_Date__c+1);
            scheduleDate =myDateTime.format('MM/dd/yyyy');  
        }
        
        if(opportunity.Event_Date__c != null)
        {
            myDateTime = (DateTime) opportunity.Event_Date__c;
            year = myDateTime.year();
            dayOfWeek = myDateTime.day()+1;
            month = myDateTime.format('MMMM');  
        }
        
        scheduleData = new Map<String, Map<Decimal, Date>>();
        
        if(numberofinstallment >= 1){
        scheduleData.put('First', new map<Decimal, Date>());
        scheduleData.get('First').put(opportunity.Deposit_Schedule_First__c,opportunity.First_Schedule_Due_Date__c);
            }
        
        if(numberofinstallment >= 2){
        scheduleData.put('Second', new map<Decimal, Date>());
        scheduleData.get('Second').put(opportunity.Deposit_Schedule_Second__c,opportunity.Second_Schedule_Due_Date__c);
        }
        
        if(numberofinstallment >= 3){
        scheduleData.put('Third', new map<Decimal, Date>());
        scheduleData.get('Third').put(opportunity.Third_Deposit_Schedule__c,opportunity.Third_Schedule_Due_Date__c);
        }
        
        if(numberofinstallment >= 4){
        scheduleData.put('Fourth', new map<Decimal, Date>());
        scheduleData.get('Fourth').put(opportunity.Fourth_Deposit_Schedule__c,opportunity.Fourth_Schedule_Due_Date__c);
        }
        
        if(numberofinstallment >= 5){
        scheduleData.put('Fifth', new map<Decimal, Date>());
        scheduleData.get('Fifth').put(opportunity.Fifth_Deposit_Schedule__c,opportunity.Fifth_Schedule_Due_Date__c);
        }
        
        if(numberofinstallment >= 6){
        scheduleData.put('Sixth', new map<Decimal, Date>());
        scheduleData.get('Sixth').put(opportunity.Sixth_Deposit_Schedule__c,opportunity.Sixth_Schedule_Due_Date__c);
        }
        
        if(numberofinstallment >= 7){
        scheduleData.put('Seventh', new map<Decimal, Date>());
        scheduleData.get('Seventh').put(opportunity.Seventh_Deposit_Schedule__c,opportunity.Seventh_Schedule_Due_Date__c);
        }
        
        if(numberofinstallment >= 8){
        scheduleData.put('Eighth', new map<Decimal, Date>());
        scheduleData.get('Eighth').put(opportunity.Eighth_Deposit_Schedule__c,opportunity.Eighth_Schedule_Due_Date__c);
        }
        
        if(numberofinstallment >= 9){
        scheduleData.put('Ninth', new map<Decimal, Date>());
        scheduleData.get('Ninth').put(opportunity.Ninth_Deposit_Schedule__c,opportunity.Ninth_Schedule_Due_Date__c);
        }
        
        if(numberofinstallment >= 10){
        scheduleData.put('Tenth', new map<Decimal, Date>());
        scheduleData.get('Tenth').put(opportunity.Tenth_Deposit_Schedule__c,opportunity.Tenth_Schedule_Due_Date__c);
        }
        
        if(numberofinstallment >= 11){
        scheduleData.put('Eleventh', new map<Decimal, Date>());
        scheduleData.get('Eleventh').put(opportunity.Eleventh_Deposit_Schedule__c,opportunity.Eleventh_Schedule_Due_Date__c);
        }
        
        if(numberofinstallment >= 12){
        scheduleData.put('Twelfth', new map<Decimal, Date>());
        scheduleData.get('Twelfth').put(opportunity.Twelfth_Deposit_Schedule__c,opportunity.Twelfth_Schedule_Due_Date__c);
        } 
        
    }
    
    public void generatePDF() {
        
        if(ApexPages.currentPage().getParameters().containsKey('generatepdf')) {
            return;
        }
        
        List<Attachment> attachListToDel = [SELECT Id,ParentId FROM Attachment WHERE ParentId =: id];
        delete attachListToDel;
        
        PageReference pdfPage = ApexPages.currentPage();
        //pdfPage.setRedirect(true);
        pdfPage.getParameters().put('generatepdf','true');
        
        Attachment attach = new Attachment();
        Blob pdfBlob;
        try {
            pdfBlob = pdfPage.getContent();
        }catch (VisualforceException e) {
            pdfBlob = Blob.valueOf('Error Uploading' + e.getMessage());
        }
        
        attach.parentId = id;
        attach.Name = opportunity.Name  + '.pdf';
        attach.body = pdfBlob;
        insert attach;
        
    }
    
    
    
}