public class qb_invoice_create_action {
    
    public class inputValues{
        @InvocableVariable(label='Invoice ID' required=true)
        public string invID;
        @InvocableVariable( label = 'Status' required = true)
        public String Status;
    }
    
    @InvocableMethod(label='Create Invoice in QB') 
    public static void createInvoice(List<inputValues> inputs){
        
        set<ID> invIDs = new set<ID>();
        
        for(inputValues i : inputs) {
            qb_invoice_create updateJob = new qb_invoice_create(i.invID, i.Status);
            ID jobID = System.enqueueJob(updateJob); 
        }  
    }
}