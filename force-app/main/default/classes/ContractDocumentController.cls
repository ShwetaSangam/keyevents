public class ContractDocumentController {
    
    public ContractDocumentController(ApexPages.StandardController standardPageController) {
        //OpportunityId = ApexPages.currentPage().getParameters().get('Id');   
        }
    
    public String getPrintView()
    {
        return
        
            '<xml>' +
            '<w:WordDocument>' +
            '<w:View>Print</w:View>' +
            '<w:Zoom>100</w:Zoom>' +
            '<w:DoNotOptimizeForBrowser/>' +
            '</w:WordDocument>' +
            '</xml>' 
            ;
        }

}