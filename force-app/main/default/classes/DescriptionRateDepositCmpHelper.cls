public with sharing class DescriptionRateDepositCmpHelper {
    
        @AuraEnabled( cacheable = true )
    public static List< Invoice__c  > fetchFields(Id invoiceId) {
     
        System.debug('id is'+ invoiceId);
        return [ SELECT  Description_1__c, Rate_1__c,Description_2__c, Rate_2__c  FROM Invoice__c WHERE Id =:invoiceId];
         
    }
    

}