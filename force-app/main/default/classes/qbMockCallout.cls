public class qbMockCallout implements HttpCalloutMock {
    
    public String response;
    
    public qbMockCallout(String response){
        this.response = response;
    }
    
    public HttpResponse respond(HTTPRequest req){
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setStatus('OK');
        res.setBody(response);
        return res;
    }
}