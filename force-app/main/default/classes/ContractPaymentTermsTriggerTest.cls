@isTest
public class ContractPaymentTermsTriggerTest {
    
    static testMethod void testMethod1() {  
        
        Account testAccount = new Account();
        testAccount.Name = 'Test Account';
        testAccount.Type = 'Prospect';
        testAccount.Industry = 'Automative';
        insert testAccount;
        
        Contact con = new Contact();
        con.LastName='Raktim1';
        con.AccountId=testAccount.Id;
        insert con; 
        
        Opportunity testOpportunity = new Opportunity();
        testOpportunity.Name = 'Testing';
        testOpportunity.AccountId = testAccount.Id;
        testOpportunity.CloseDate = System.today();
        testOpportunity.type = 'Existing Business';
        testOpportunity.Client_Name1__c = con.Id;
        testOpportunity.StageName='Discovery';
        testOpportunity.LeadSource='Customer Referral';
        testOpportunity.Commencement_of_Agreement_Date__c=System.today();
        testOpportunity.Start_Date__c=System.today();
        testOpportunity.Event_Date__c = System.today();
        testOpportunity.Deposit_Schedule_Due_Date__c = System.today();
        testOpportunity.End_Date__c = System.today();
        testOpportunity.Amount=10000;
        testOpportunity.Contract_Type__c='Monthly';
        testOpportunity.Payment_Term__c='12';
        insert testOpportunity;
        
        testOpportunity.Contract_Type__c='Custom';
        testOpportunity.Payment_Term__c='3';
        update testOpportunity;
        
        testOpportunity.Contract_Type__c='Custom';
        testOpportunity.Payment_Term__c='6';
        update testOpportunity;
        
        testOpportunity.Contract_Type__c='Custom';
        testOpportunity.Payment_Term__c='12';
        update testOpportunity;
        
        testOpportunity.Contract_Type__c='Custom';
        testOpportunity.Payment_Term__c='8';
        update testOpportunity;
        
        testOpportunity.Contract_Type__c='Yearly';
        testOpportunity.Payment_Term__c='1';
        update testOpportunity;
        
        testOpportunity.Contract_Type__c='Quaterly';
        testOpportunity.Payment_Term__c='4';
        update testOpportunity;
        
        testOpportunity.Contract_Type__c='Half Yearly';
        testOpportunity.Payment_Term__c='2';
        update testOpportunity;
        
        testOpportunity.Contract_Type__c='Percentage';
        testOpportunity.Payment_Term__c='2';
        testOpportunity.Percentage_No_1__c=50;
        testOpportunity.Percentage_No_2__c=50;
        testOpportunity.Percentage_No_3__c=0;
        testOpportunity.Percentage_No_4__c=0;
        testOpportunity.Percentage_No_5__c=0;
        update testopportunity; 
        
        testOpportunity.Contract_Type__c='Percentage';
        testOpportunity.Payment_Term__c='3';
        update testopportunity;
        
        testOpportunity.Percentage_No_1__c=25;
        testOpportunity.Percentage_No_2__c=25;
        testOpportunity.Percentage_No_3__c=50;
        update testopportunity;
        
        testOpportunity.Contract_Type__c='Percentage';
        testOpportunity.Payment_Term__c='4';
        update testopportunity;
        
        testOpportunity.Percentage_No_1__c=25;
        testOpportunity.Percentage_No_2__c=25;
        testOpportunity.Percentage_No_3__c=25;
        testOpportunity.Percentage_No_4__c=25;
        update testopportunity;
        
        testOpportunity.Contract_Type__c='Percentage';
        testOpportunity.Payment_Term__c='5';
        update testopportunity;
        
        testOpportunity.Percentage_No_1__c=20;
        testOpportunity.Percentage_No_2__c=20;
        testOpportunity.Percentage_No_3__c=20;
        testOpportunity.Percentage_No_4__c=20;
        testOpportunity.Percentage_No_5__c=20;
        update testopportunity;
        
        testOpportunity.Contract_Type__c=null;
        testOpportunity.Payment_Term__c=null;
        update testopportunity;
        
    }
}