public class qb_customerupdate_action {
    
    public class inputValues{
        @InvocableVariable(label='Account ID' required=true)
        public string accID;
    }
    
    @InvocableMethod(label='Update Customer in QB') 
    public static void updateCustomer(List<inputValues> inputs){
        
        set<ID> appIDs = new set<ID>();
        
        for(inputValues i : inputs) {
            qb_customerupdate updateJob = new qb_customerupdate(i.accID);
            ID jobID = System.enqueueJob(updateJob); 
        }  
    }
}