public with sharing class UpdatePercentageController {
    
    public Opportunity opportunity {get; set;}
    public String OpportunityId; 
    
    public UpdatePercentageController(ApexPages.StandardController standardPageController) {
        OpportunityId = ApexPages.currentPage().getParameters().get('Id');   
        }
  
  	public void updatePercentage() {
        
        this.opportunity = [SELECT Id, Update_Percentage_Values__c
                        FROM Opportunity
                        WHERE Id =: OpportunityId
                       ];
        
        opportunity.Update_Percentage_Values__c = true;
        
        update opportunity;
    }

}