@RestResource(urlmapping = '/NewVendor/*')
global class NewVendor {
    
    @HttpPost
    global static void CreateNewVendor(){
        
        VendorCreation body = new VendorCreation();
        body = (VendorCreation)System.JSON.deserialize(RestContext.request.requestBody.tostring(), VendorCreation.class);
        String RecordT = 'Vendor';
        RecordType newrt = [Select Id from RecordType where DeveloperName =: RecordT limit 1 ];
       List<Account> AccList = new List<Account>([Select Id from Account where Phone =: body.PrimaryPhone.FreeFormNumber OR Email__c =: body.PrimaryEmailAddr.Address]);
        
	    
        if(AccList.size()>0){
            RestResponse res1 = RestContext.response;
            res1.statusCode = 200;
            res1.responseBody = Blob.valueOf('Account already exists');
        }
        
        else{
            
            Account Acc = new Account();
            Acc.Name = body.DisplayName;
            Acc.BillingStreet = body.BillAddr.Line1 + ', ' + body.BillAddr.Line2 + ', ' + body.BillAddr.Line1;
            Acc.BillingCity = body.BillAddr.City;
            Acc.BillingCountry = body.BillAddr.Country;
            Acc.BillingPostalCode = body.BillAddr.PostalCode;
            Acc.BillingState = body.BillAddr.CountrySubDivisionCode;
            Acc.Phone = body.PrimaryPhone.FreeFormNumber;
            Acc.Email__c = body.PrimaryEmailAddr.Address;
            Acc.RecordTypeId = newrt.Id;
            Acc.Type = 'Vendor';
            
            RestResponse res = RestContext.response;
            
                try{
                    insert Acc;
                    res.responseBody = Blob.valueOf(JSON.serialize(Acc.id));
                    res.statusCode = 200;
                }
                CATCH(Exception e){
                    res.responseBody = Blob.valueOf(e.getMessage());
                    res.statusCode = 500;
                }
            
        }
    }
    public class VendorCreation{
        public string Title {get; set;}
        public string Suffix {get; set;}
        public String DisplayName {get; set;}
        public Address BillAddr {get; set;}
        public Phonedetails PrimaryPhone {get;set;}
        public EmailDetails PrimaryEmailAddr {get;set;}
        public Web WebAddr {get;set;}
    }
    
    public class Address{
        public String Line1 {get; set;}
        public String Line2 {get; set;}
        public String Line3 {get; set;}
        public String City {get; set;}
        public String Country {get; set;}
        public String CountrySubDivisionCode {get; set;}
        public String PostalCode {get; set;}
    }
    
    public class Phonedetails{
        public String FreeFormNumber {get;set;}
    }
    
    public class EmailDetails{
        public string Address{get;set;}
    }
    
    public class Web{
        public String URI{get;set;}
    }
}