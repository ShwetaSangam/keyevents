@isTest
public class ContractPdfControllerTest 
{
     static testMethod void testMethod1() 
     {
        Account testAccount = new Account();
        testAccount.Name = 'Test Account';
        testAccount.Type = 'Prospect';
        testAccount.Industry = 'Automative';
        insert testAccount;
     	
         Contact con = new Contact();
         con.LastName='Raktim1';
         
         con.AccountId=testAccount.Id;
         insert con;
         
        Opportunity testOpportunity = new Opportunity();
        testOpportunity.Name = 'Testing';
        testOpportunity.AccountId = testAccount.Id;
        testOpportunity.CloseDate = System.today();
        testOpportunity.type = 'Existing Business';
        testOpportunity.Client_Name1__c = con.Id;
        testOpportunity.StageName='Discovery';
        testOpportunity.LeadSource='Customer Referral';
        testOpportunity.Commencement_of_Agreement_Date__c=System.today();
        testOpportunity.Start_Date__c=System.today();
        testOpportunity.Event_Date__c = System.today();
        testOpportunity.Deposit_Schedule_Due_Date__c = System.today();
        testOpportunity.End_Date__c = System.today();
        insert testOpportunity;
         
         Test.StartTest(); 
         ApexPages.StandardController sc = new ApexPages.StandardController(testOpportunity);
        PageReference pageRef = Page.KeyEventsContractVFpage;
        Test.setCurrentPage(pageRef);
        
        ApexPages.currentPage().getParameters().put('id', String.valueOf(testOpportunity.Id));
      
        ContractPdfController controller = new ContractPdfController(sc);
        
        controller.clientname = Null;
        controller.phonenumber = null;
        controller.email = null;
        controller.generatePDF();
        
     Test.StopTest();
     }

}